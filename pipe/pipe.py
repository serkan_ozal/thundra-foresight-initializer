from bitbucket_pipes_toolkit import Pipe, get_logger, os
from jinja2 import Template
import xml.etree.ElementTree as ET
import urllib3
import fnmatch
import shutil
import subprocess

logger = get_logger()

schema = {
    'NAME': {'type': 'string', 'required': False},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}

THUNDRA_AGENT_URL = "https://repo.thundra.io/service/local/repositories/thundra-releases/content/io/thundra/agent/thundra-agent-bootstrap/{agentVersion}/thundra-agent-bootstrap-{agentVersion}.jar";
THUNDRA_MAVEN_PLUGIN_URL = "https://repo1.maven.org/maven2/io/thundra/plugin/thundra-agent-maven-test-instrumentation/{mavenInstrumenterVersion}/thundra-agent-maven-test-instrumentation-{mavenInstrumenterVersion}.jar";
THUNDRA_AGENT_METADATA = 'https://repo.thundra.io/service/local/repositories/thundra-releases/content/io/thundra/agent/thundra-agent-bootstrap/maven-metadata.xml';
MAVEN_INSTRUMENTATION_METADATA = 'https://repo1.maven.org/maven2/io/thundra/plugin/thundra-agent-maven-test-instrumentation/maven-metadata.xml';
THUNDRA_AGENT_BOOTSTRAP_JAR = "thundra-agent-bootstrap.jar"
THUNDRA_AGENT_MAVEN_PLUGIN_JAR = "thundra-agent-maven-test-instrumentation.jar"
GRADLE_PLUGIN_METADATA = "https://repo1.maven.org/maven2/io/thundra/plugin/thundra-gradle-test-plugin/maven-metadata.xml"

class DemoPipe(Pipe):
    agentPath = ""
    mavenJarPath = ""

    def findDependencyVersion(self, dependency):
        http = urllib3.PoolManager()
        r = http.request('GET', dependency)

        root = ET.fromstring(r.data)
        for latest in root.iter('latest'):
            return latest.text;

    def replace_pomfiles(self, pomFileList):
        mavenPluginVersion = pipe.findDependencyVersion(MAVEN_INSTRUMENTATION_METADATA)
        if mavenPluginVersion is None:
            exit(-1)
        pipe.download_maven_plugin(mavenPluginVersion)
        if pipe.mavenJarPath is None:
            exit(-1)
        str1 = " "
        subprocess.call(['java', '-jar', pipe.mavenJarPath, pipe.agentPath, str1.join(pomFileList)])

        for pom in pomFileList:
            with open(pom) as f:
                contents = f.read()
                logger.info("read pom file start: " + pom)
                print(contents)
                logger.info("read pom file end")


    def downloadAgent(self, agentVersion):
        http = urllib3.PoolManager()
        with open(THUNDRA_AGENT_BOOTSTRAP_JAR, 'wb') as out:
            file = http.request('GET', THUNDRA_AGENT_URL.format(agentVersion=agentVersion), preload_content=False)
            shutil.copyfileobj(file, out)
            pipe.agentPath = os.path.realpath(out.name)

        logger.info("Agent Path : ")
        print(pipe.agentPath)

    def download_maven_plugin(self, mavenPluginVersion):
        http = urllib3.PoolManager()
        with open(THUNDRA_AGENT_MAVEN_PLUGIN_JAR, 'wb') as out:
            file = http.request('GET', THUNDRA_MAVEN_PLUGIN_URL.format(mavenInstrumenterVersion=mavenPluginVersion), preload_content=False)
            shutil.copyfileobj(file, out)
            pipe.mavenJarPath = os.path.realpath(out.name)


    def run(self):
        super().run()
        agent_version = pipe.findDependencyVersion(THUNDRA_AGENT_METADATA)

        if agent_version is None:
            print('Agent version could not find')
            exit(-1)
        else:
            pipe.downloadAgent(agent_version)
        logger.info('Executing the pipe...')
        pomFileList = []
        for dirpath, dirs, files in os.walk(os.environ['BITBUCKET_CLONE_DIR']):
            for filename in fnmatch.filter(files, 'pom.xml'):
                pomFileList.append(os.path.join(dirpath, filename))
        gradleFileList = []
        for dirpath, dirs, files in os.walk(os.environ['BITBUCKET_CLONE_DIR']):
            for filename in fnmatch.filter(files, 'build.gradle'):
                gradleFileList.append(os.path.join(dirpath, filename))

        if (len(pomFileList) > 0):
            pipe.replace_pomfiles(pomFileList)
        if (len(gradleFileList) > 0):
            pipe.create_gradle_init_script()

        self.success(message="Success!")
    def create_gradle_init_script(self):
        content = None
        gradle_plugin_version = pipe.findDependencyVersion(GRADLE_PLUGIN_METADATA)

        with open("/thundrainit.template", "r") as f:
            t = Template(f.read())
            content = t.render(THUNDRA_GRADLE_PLUGIN_VERSION=gradle_plugin_version, THUNDRA_AGENT_PATH=pipe.agentPath);
            f.close()
        file = os.environ['BITBUCKET_CLONE_DIR'] + "/thundrainit.gradle"
        with open(file, "w") as f:
            f.write(content)
            f.close()

if __name__ == '__main__':
    pipe = DemoPipe(pipe_metadata='/pipe.yml', schema=schema)
    pipe.run()
